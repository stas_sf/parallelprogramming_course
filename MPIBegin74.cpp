#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin74");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Comm new_comm;

    int sz = (size + 1) / 2;
    int *ranks = new int[sz];
    for (int i = 0; i < sz; ++i)
        ranks[i] = i * 2;

    MPI_Group group, world_group;
    MPI_Comm_group(MPI_COMM_WORLD, &world_group);
    MPI_Group_incl(world_group, sz, ranks, &group);
    MPI_Comm_create(MPI_COMM_WORLD, group, &new_comm);

    if (new_comm != MPI_COMM_NULL)
    {
        double* sbuf = new double[3];
        for (int i = 0; i < 3; ++i)
            pt >> sbuf[i];
        double* rbuf = new double[3];
        MPI_Reduce(sbuf, rbuf, 3, MPI_DOUBLE, MPI_MIN, 0, new_comm);
        if (!rank)
            for (int i = 0; i < 3; ++i)
                pt << rbuf[i];
    }
}
