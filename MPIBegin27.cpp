#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin27");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Status s;
    double a;
    int n;
	int l;
    if (rank > 0)
    {
        pt >> l;
        for (int i = 0; i < l; ++i)
        {
            pt >> a;
            pt >> n;
            MPI_Send(&a, 1, MPI_DOUBLE, 0, n, MPI_COMM_WORLD);
        }
    }
    else
    {
        int count = size*2;
        double* nums = new double[count];
        a = 1.9;
        for (int i = 0; i < count; ++i)
        {
            MPI_Recv(&a, 1, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &s);
            nums[s.MPI_TAG-1] = a;
        }
        for (int i = 0; i < count; ++i)
        {
            pt << nums[i];
        }
    }
}
