#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin95");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    double d, r;

	if (rank)
		pt >> d;

	int dims[1] = {size};
	int periods[1] = {0};
	MPI_Comm comm;
	MPI_Cart_create(MPI_COMM_WORLD, 1, dims, periods, 0, &comm);

	int r_source, r_dest;
	MPI_Cart_shift(comm, 0, -1, &r_source, &r_dest);

	MPI_Send(&d, 1, MPI_DOUBLE, r_dest, 0, comm);
	
	MPI_Status s;
	MPI_Recv(&r, 1, MPI_DOUBLE, r_source, 0, comm, &s);

	if (rank != size - 1)
		pt << r;
}
