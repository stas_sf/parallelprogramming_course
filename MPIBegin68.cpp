 #include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin68");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    char buff[200];
    int position = 0, a, b;
    double c;
    int count, icount, dcount;
    MPI_Pack_size((size - 1), MPI_INT, MPI_COMM_WORLD, &icount);
    MPI_Pack_size(size - 1, MPI_DOUBLE, MPI_COMM_WORLD, &dcount);
    Show(icount);
    ShowLine(dcount);
    count = icount + dcount;
    //int count = (2*sizeof(MPI_INT) + sizeof(MPI_DOUBLE))*(size+1);

    if (rank == 0)
    {
        position = 0;
        for (int i = 0; i < size - 1; ++i)
        {
            pt >> a;
            pt >> b;
            pt >> c;
            MPI_Pack(&a, 1, MPI_INT, buff, 100, &position, MPI_COMM_WORLD);
            MPI_Pack(&b, 1, MPI_INT, buff, 100, &position, MPI_COMM_WORLD);
            MPI_Pack(&c, 1, MPI_DOUBLE, buff, 100, &position, MPI_COMM_WORLD);
        }
    }
    Show(position);
    Show(count);
    MPI_Bcast(buff, count, MPI_PACKED, 0, MPI_COMM_WORLD);

    if (rank != 0)
    {
        position = 0;
        for (int i = 0; i < size - 1; ++i)
        {
            MPI_Unpack(buff, 100, &position, &a,  1, MPI_INT, MPI_COMM_WORLD);
            MPI_Unpack(buff, 100, &position, &b,  1, MPI_INT, MPI_COMM_WORLD);
            MPI_Unpack(buff, 100, &position, &c, 1, MPI_DOUBLE, MPI_COMM_WORLD);
            pt << a << b << c;
        }
    }
}
