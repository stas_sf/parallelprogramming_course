#include <windows.h>
#pragma hdrstop
#include "pt4.h"
#include "omp.h"

void Solve()
{
    Task("OMPBegin15");

    double t1, t2, npTime;

	double x;
	int n;

	pt >> x >> n;

	double f = 0;

	t1 = omp_get_wtime();

	for (int i = 1; i < n + 1; ++i)
	{
		double denom = 0;
		for (int j = i; j < n + 1; ++j)
		{
			denom += (j + pow(x + j, (1.0 / 4.0))) / (2 * i * j - 1);
		}
		f += 1 / denom;
	}

	t2 = omp_get_wtime();
	npTime = t2 - t1;

	pt << f;

	ShowLine("Non-parallel time: ", npTime);

	pt >> x >> n;
	
	int num;
	double g = 0;
	double pTime;

	omp_set_dynamic(0);
	omp_set_num_threads(4);

	int k, k0, k1, k2, k3;

	k = int(double(n * (n + 1)) / 2.0);
	k0 = k / 4;

	int count = 0;
	int i = 0;
	while (count < k0)
	{
		count += n - i;
		++i;
	}
	k1 = i;
	count = 0;
	while (count < k0)
	{
		count += n - i;
		++i;
	}
	k2 = i;
	count = 0;
	while (count < k0)
	{
		count += n - i;
		++i;
	}
	k3 = i;
	count = 0;

	t1 = omp_get_wtime();
#pragma omp parallel reduction (+: g) private(num)
	{
#pragma omp master
		{
			int num_procs = omp_get_num_procs();
			int num_threads = omp_get_num_threads();

			ShowLine("num_procs: ", num_procs);
			ShowLine("num_threads: ", num_threads);
		}

		double thr_t1, thr_t2, thr_t;
		int c = 0;
		num = omp_get_thread_num();

		int k = int(n / sqrt(2)) + 1;

		thr_t1 = omp_get_wtime();

#pragma omp sections
		{
#pragma omp section
			{
				for (int i = 1; i < k1 + 1; ++i)
				{
					double denom = 0;
                    for (int j = i; j < n + 1; ++j)
                    {
                        denom += (j + pow(x + j, (1.0 / 4.0))) / (2 * i * j - 1);
                    }
                    g += 1 / denom;
				}
			}
#pragma omp section
			{
				for (int i = k1 + 1; i < k2 + 1; ++i)
				{
					double denom = 0;
                    for (int j = i; j < n + 1; ++j)
                    {
                        denom += (j + pow(x + j, (1.0 / 4.0))) / (2 * i * j - 1);
                    }
                    g += 1 / denom;
				}
			}
#pragma omp section
			{
				for (int i = k2 + 1; i < k3 + 1; ++i)
				{
					double denom = 0;
                    for (int j = i; j < n + 1; ++j)
                    {
                        denom += (j + pow(x + j, (1.0 / 4.0))) / (2 * i * j - 1);
                    }
                    g += 1 / denom;
				}
			}
#pragma omp section
			{
				for (int i = k3 + 1; i < n + 1; ++i)
				{
					double denom = 0;
                    for (int j = i; j < n + 1; ++j)
                    {
                        denom += (j + pow(x + j, (1.0 / 4.0))) / (2 * i * j - 1);
                    }
                    g += 1 / denom;
				}
			}
		}
		thr_t2 = omp_get_wtime();
		thr_t = thr_t2 - thr_t1;

#pragma omp critical
		{
			Show("thread_num: ", num);
			Show("Count: ", c);
			ShowLine("Thread time: ", thr_t);
		}
	}
	t2 = omp_get_wtime();
	pTime = t2 - t1;

	pt << g;

	ShowLine("Total parallel time: ", pTime);
	ShowLine("Rate: ", npTime / pTime);
}














































//-----------------------------------------------------------//
//        ����������� ������� ��������� ���������            //
//     ����� �������� � ������������ ������ ���������!       //
//-----------------------------------------------------------//
/*int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{ _try {Solve();}
  _except (Filter(GetExceptionInformation())){};
  return 0;
}*/
//-----------------------------------------------------------//
