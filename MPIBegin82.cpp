#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin82");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int n;
	pt >> n;

    double a;
	pt >> a;

	MPI_Comm comm;
	MPI_Comm_split(MPI_COMM_WORLD, n, 0, &comm);
		
	double res;
    MPI_Op op = n == 1 ? MPI_MIN : MPI_MAX;
	MPI_Allreduce(&a, &res, 1, MPI_DOUBLE, op, comm);

	pt << res;
}
