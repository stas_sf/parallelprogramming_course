#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin84");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int dims[2];
	int n;
	if (!rank)
		pt >> n;

	MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

	dims[0] = n;
	dims[1] = size / n;

	int flags[2] = { 0, 0 };

	MPI_Comm cart;
	MPI_Cart_create(MPI_COMM_WORLD, 2, dims, flags, 0, &cart);

	int coord[2];
	int crank;
	if (cart != MPI_COMM_NULL)
	{
		MPI_Comm_rank(cart, &crank);
		MPI_Cart_coords(cart, crank, 2, coord);
		pt << coord[0] << coord[1];
	}
}
