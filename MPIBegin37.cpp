#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin37");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	double* sbuf = new double[size];
	if (rank == 0)
		for (int i = 0; i < size; ++i)
			pt >> sbuf[i];

	int* scounts = new int[size];
	int* disps = new int[size];
	for (int i = 0; i < size; ++i)
	{
		scounts[i] = 1;
		disps[i] = size - i - 1;
	}
	double rbuf[100];

	MPI_Scatterv(sbuf, scounts, disps, MPI_DOUBLE, rbuf, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	pt << rbuf[0];

	delete[] sbuf;
	delete[] scounts;
	delete[] disps;

}














































//--------------------------------------------------//
//     WARNING! Altering this part of program       //
//  may cause Programming Taskbook to malfunction.  //
//--------------------------------------------------//
#ifdef PT4_CODEBLOCKS
#include "seh.h"
int main()
{
    __seh_try{Solve();}
    __seh_except(Filter(GetExceptionCode())){}
    __seh_end_except
    return 0;
}
#else
#pragma warning (disable:4430)
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    _try 
    {
         Solve();
    }
    _except(Filter(GetExceptionInformation()))
    {}  
    return 0;
}
#pragma warning (default:4430)
#endif
//--------------------------------------------------//


