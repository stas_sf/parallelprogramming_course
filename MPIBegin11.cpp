#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin11");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	double d;
	MPI_Status s;
	if (rank == 0)
	{
		for (int i = 1; i < size; ++i)
		{
			pt >> d;
			MPI_Send(&d, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
		}
	}
	else
	{
		MPI_Recv(&d, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &s);
		pt << d;
	}
}
