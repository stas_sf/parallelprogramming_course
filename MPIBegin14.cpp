#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin14");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Status s;
	int a;
	if (rank > 0)
	{
		pt >> a;
		if (a != 0)
			MPI_Send(&a, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
	}
	else
	{
		MPI_Recv(&a, 1, MPI_DOUBLE, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &s);
		pt << a;
		pt << s.MPI_SOURCE;
	}

}
