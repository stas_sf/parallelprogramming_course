#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin86");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int dims[2] = { size / 2, 2 };
	int flags[2] = { 0, 0 };

	MPI_Comm cart;
	MPI_Cart_create(MPI_COMM_WORLD, 2, dims, flags, 0, &cart);

	int cut[2] = { 1, 0 };
	MPI_Comm comm;
	MPI_Cart_sub(cart, cut, &comm);

	int r;
	MPI_Comm_rank(comm, &r);
	
	double a;
	if (!r)
		pt >> a;

	MPI_Bcast(&a, 1, MPI_DOUBLE, 0, comm);
	pt << a;
}
