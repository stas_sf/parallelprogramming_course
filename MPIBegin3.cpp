#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin3");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if (rank % 2 != 0)
	{
		double n;
		pt >> n;
		pt << 2 * n;
	}
	else
	{
		int n;
		pt >> n;
		pt << 2 * n;
	}
	// or better solution should be like next
	// double d; int n; if (odd()) { pt >> d; ... } else { pt >> n; ... }
	// ???
}
