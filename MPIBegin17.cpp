#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin17");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	double a;
	MPI_Status s;
	if (rank > 0)
	{
		pt >> a;
		MPI_Send(&a,1,MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
		MPI_Recv(&a, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &s);
		pt << a;
	}
	else
	{
		pt >> a;
		for (int i = 1; i < size; ++i)
		{
			double b;
			MPI_Recv(&b, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, &s);
			pt << b;
			MPI_Send(&a, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
		}
	}
}
