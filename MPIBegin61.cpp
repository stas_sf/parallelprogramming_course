#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"

struct IntIntDouble {
    int a, b;
    double c;
};

void Build_derived_type(/*IntIntDouble* indata,*/ MPI_Datatype* type_ptr)
{
    int block_lengths[3];
    MPI_Aint displacements[3];
    MPI_Aint addresses[4];
    MPI_Datatype typelist[3];

    typelist[0] = MPI_INT;
    typelist[1] = MPI_INT;
    typelist[2] = MPI_DOUBLE;
    
    block_lengths[0] = block_lengths[1] = block_lengths[2] = 1;
    /*
    MPI_Address(indata, &addresses[0]);
    MPI_Address(&(indata->a), &addresses[1]);
    MPI_Address(&(indata->b), &addresses[2]);
    MPI_Address(&(indata->c), &addresses[3]);*/
    displacements[0] = 0;
    displacements[1] = sizeof(MPI_INT);
    displacements[2] = sizeof(MPI_INT) + displacements[1];
    
    MPI_Type_struct(3, block_lengths, displacements, typelist, type_ptr);
    
    MPI_Type_commit(type_ptr);
}

void Solve()
{
    Task("MPIBegin61");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Datatype type_ptr;
    Build_derived_type(&type_ptr);

    IntIntDouble* buf = new IntIntDouble[size-1];

    if (rank == 0)
    {
        for (int i = 0; i < size - 1; ++i)
        {
            pt >> buf[i].a;
            pt >> buf[i].b;
            pt >> buf[i].c;
        }
    }

    MPI_Bcast(buf, size - 1, type_ptr, 0, MPI_COMM_WORLD);

    if (rank > 0)
    {
        for (int i = 0; i < size - 1; ++i)
        {
            pt << buf[i].a << buf[i].b << buf[i].c;
        }
    }

}
