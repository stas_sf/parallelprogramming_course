#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin24");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Status s;
	int n, m;
    pt >> n;
    double* a = new double[n];
    for (int i = 0; i < n; ++i)
        pt >> a[i];
    int proc;
    if (rank % 2) // if rank is odd
        proc = rank - 1;
    else
        proc = rank + 1;
    
    MPI_Send(&n, 1, MPI_INT, proc, 0, MPI_COMM_WORLD);
    MPI_Recv(&m, 1, MPI_INT, proc, 0, MPI_COMM_WORLD, &s);

    double* b = new double[m];

    MPI_Sendrecv(a, n, MPI_DOUBLE, proc, 0, b, m, MPI_DOUBLE, proc, 0, MPI_COMM_WORLD, &s);

    for (int i = 0; i < m; ++i)
        pt << b[i];
    delete a;
    delete b;
}
