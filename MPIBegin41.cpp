#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin41");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int sbuf[4];
    for (int i = 0; i < 4; ++i)
    {
        pt >> sbuf[i];
    }
    int count = size*4;
    int rbuf[100];
    MPI_Allgather(sbuf, 4, MPI_INT, rbuf, 4, MPI_INT, MPI_COMM_WORLD);
    for (int i = 0; i < count; ++i)
        pt << rbuf[i];

}
