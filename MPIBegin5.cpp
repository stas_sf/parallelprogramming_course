#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin5");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	int n;
	pt >> n;
	double sum = 0;
	int count = 0;
	for (int i = 0; i < n; ++i)
	{
		double d;
		pt >> d;
		//even -> sum
		sum += d;
		//odd -> average
		if (rank % 2 != 0)
			++count;
	}
	if (rank % 2 == 0)
		pt << sum;
	else
		pt << sum / count;
}
