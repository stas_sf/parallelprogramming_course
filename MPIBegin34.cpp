#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin34");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	int count = rank + 2;
	int* sbuf = new int[count];
	for (int i = 0; i < count; ++i)
	{
		pt >> sbuf[i];
	}
	int rbuf[100];
	int* rcounts = new int[size];
	int* disps = new int[size];
	disps[0] = 0;
	rcounts[0] = 2;
	for (int i = 1; i < size; ++i)
	{
		rcounts[i] = i + 2;
		disps[i] = disps[i-1] + rcounts[i-1];
	}
	MPI_Gatherv(sbuf, count, MPI_INT, rbuf, rcounts, disps, MPI_INT, 0,  MPI_COMM_WORLD);
	if (rank == 0)
	{
		for (int i = 0; i < size; ++i)
		{
			for (int j = 0; j < rcounts[i]; ++j)
			{
				pt << rbuf[disps[i]+j];
			}
		}
	}
	delete[] rcounts;
	delete[] disps;
	delete[] sbuf;
}














































//--------------------------------------------------//
//     WARNING! Altering this part of program       //
//  may cause Programming Taskbook to malfunction.  //
//--------------------------------------------------//
#ifdef PT4_CODEBLOCKS
#include "seh.h"
int main()
{
    __seh_try{Solve();}
    __seh_except(Filter(GetExceptionCode())){}
    __seh_end_except
    return 0;
}
#else
#pragma warning (disable:4430)
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    _try 
    {
         Solve();
    }
    _except(Filter(GetExceptionInformation()))
    {}  
    return 0;
}
#pragma warning (default:4430)
#endif
//--------------------------------------------------//


