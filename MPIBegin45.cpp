#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin45");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int count = (size * (size + 1)) / 2;
	int* sbuf = new int[count];
	for (int i = 0; i < count; ++i)
	{
		pt >> sbuf[i];
		//Show(sbuf[i]);
	}
	//ShowLine();

	int* scounts = new int[size];
	int* sdisps = new int[size];
	int* rcounts = new int[size];
	int* rdisps = new int[size];
	int disp = 0;
	scounts[0] = 1;
	sdisps[0] = 0;
	rcounts[0] = rank+1;
	rdisps[0] = 0;

	for (int i = 1; i < size; ++i)
	{
		scounts[i] = i + 1;
		sdisps[i] = sdisps[i - 1] + scounts[i - 1];
		rcounts[i] = rank + 1;
		rdisps[i] = rdisps[i-1] + rank + 1;
	}
	int rbuf[100];

	MPI_Alltoallv(sbuf, scounts, sdisps, MPI_INT, rbuf, rcounts, rdisps, MPI_INT, MPI_COMM_WORLD);

	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < rcounts[i]; ++j)
		{
			pt << rbuf[j + rdisps[i]];
		}
	}

	delete[] scounts;
	delete[] rcounts;
	delete[] sdisps;
	delete[] rdisps;
}














































//--------------------------------------------------//
//     WARNING! Altering this part of program       //
//  may cause Programming Taskbook to malfunction.  //
//--------------------------------------------------//
#ifdef PT4_CODEBLOCKS
#include "seh.h"
int main()
{
    __seh_try{Solve();}
    __seh_except(Filter(GetExceptionCode())){}
    __seh_end_except
    return 0;
}
#else
#pragma warning (disable:4430)
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    _try 
    {
         Solve();
    }
    _except(Filter(GetExceptionInformation()))
    {}  
    return 0;
}
#pragma warning (default:4430)
#endif
//--------------------------------------------------//


