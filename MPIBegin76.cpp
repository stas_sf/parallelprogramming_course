#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin76");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Comm comm;

	int k;
	pt >> k;

    MPI_Comm_split(MPI_COMM_WORLD, k > 0 ? 1 : MPI_UNDEFINED, rank, &comm);

    if (comm == MPI_COMM_NULL)
		return;

    double* sbuf = nullptr;
	if (!rank)
	{
		sbuf = new double[k + 1];
		for (int i = 1; i < k + 1; ++i)
			pt >> sbuf[i];
	}

	double rbuf;
    MPI_Scatter(sbuf, 1, MPI_DOUBLE, &rbuf, 1, MPI_DOUBLE, 0, comm);

    delete[] sbuf;

	if (rank)
		pt << rbuf;
}
