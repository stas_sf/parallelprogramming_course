#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin18");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Status s;
	int a;
	int from, to;
	if (rank == size-1)
	{
		to = 0;
		from = rank-1;
	}
	else if (rank == 0)
	{
		from = size-1;
		to = rank+1;
	}
	else
	{
		from = rank - 1;
		to = rank + 1;
	}
	pt >> a;
	MPI_Send(&a, 1, MPI_INT, to, 0, MPI_COMM_WORLD);
	MPI_Recv(&a, 1, MPI_INT, from, 0, MPI_COMM_WORLD, &s);
	pt << a;

}
