#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin28");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0)
    {
        int t;
        for (int i = 1; i < size; ++i)
        {
            pt >> t;
            if (t == 1)
            {
                double a;
                pt >> a;
                MPI_Send(&a, 1, MPI_DOUBLE, i, t, MPI_COMM_WORLD);
            }
            else if (t == 0)
            {
                int a;
                pt >> a;
                MPI_Send(&a, 1, MPI_INT, i, t, MPI_COMM_WORLD);
            }
        }
    }
    else
    {
        MPI_Status s;
        MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &s);
        if (s.MPI_TAG == 0)
        {
            int a;
            MPI_Recv(&a, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &s);
            pt << a;
        }
        else if (s.MPI_TAG == 1)
        {
            double a;
            MPI_Recv(&a, 1, MPI_DOUBLE, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &s);
            pt << a;
        }
    }
}
