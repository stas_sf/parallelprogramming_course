#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin100");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int n = (size - 1) / 3;

	int *index = new int[size],
		*edges = new int[8 * n];

	index[0] = n;
	int j = n;
	for (int i = 1; i <= n; i++)
		edges[i - 1] = 3 * i;
	for (int i = 1; i < size; i++)
	{
		index[i] = index[i - 1] + (i % 3 == 0 ? 3 : 2);
		if (i % 3 == 1)
		{
			edges[j] = i + 1;
			edges[j + 1] = i + 2;
			j += 2;
		}
		else if (i % 3 == 2)
		{
			edges[j] = i - 1;
			edges[j + 1] = i + 1;
			j += 2;
		}
		else
		{
			edges[j] = 0;
			edges[j + 1] = i - 2;
			edges[j + 2] = i - 1;
			j += 3;
		}
	}

	MPI_Comm g_comm;
	MPI_Graph_create(MPI_COMM_WORLD, size, index, edges, 0, &g_comm);

	int count;
	MPI_Graph_neighbors_count(g_comm, rank, &count);

	if (count < 2 && rank > 0)
	   if (rank % 3 == 1)
		   count += 3;
	   else
		   count += 2;

	int *neighbors = new int[count];
	MPI_Graph_neighbors(g_comm, rank, count, neighbors);

	int a, b;
	pt >> a;
	MPI_Status s;
	for (int i = 0; i < count; ++i)
	{
		MPI_Sendrecv(&a, 1, MPI_INT, neighbors[i], 0, &b, 1, MPI_INT, neighbors[i], 0, g_comm, &s);
		pt << b;
	}
	delete[] index;
	delete[] edges;
	delete[] neighbors;
}
