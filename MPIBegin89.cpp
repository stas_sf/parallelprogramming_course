#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin89");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int num;
	pt >> num;

	int dims[3] = { 2, 2, size / 4 };
	int periods[3] = { 0, 0, 0 };

	MPI_Comm comm;
	MPI_Cart_create(MPI_COMM_WORLD, 3, dims, periods, 0, &comm);

	int remain[3] = { 0, 0, 1 };

	MPI_Comm sub_comm;
	MPI_Cart_sub(comm, remain, &sub_comm);
	MPI_Comm_rank(sub_comm, &rank);

	int rbuf[20];
	MPI_Gather(&num, 1, MPI_INT, rbuf, 1, MPI_INT, 0, sub_comm);

	if (!rank)
		for (int i = 0; i < size / 4; ++i)
			pt << rbuf[i];
}
