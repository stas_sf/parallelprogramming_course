#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin43");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    double* sbuf = new double[size];
    for (int i = 0; i < size; ++i)
        pt >> sbuf[i];
    double rbuf[100];
    MPI_Alltoall(sbuf, 1, MPI_DOUBLE, rbuf, 1, MPI_DOUBLE, MPI_COMM_WORLD);
    for (int i = 0; i < size; ++i)
        pt << rbuf[i];
}
