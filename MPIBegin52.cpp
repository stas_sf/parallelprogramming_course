#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"

struct MINLOC_Data {
	double a;
	int n;
};

void Solve()
{
    Task("MPIBegin52");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	MINLOC_Data res[10], d[10];

	for (int i = 0; i < size + 5; ++i)
	{
		pt >> d[i].a;
		d[i].n = rank;
	}

	MPI_Allreduce(d, res, size + 5, MPI_DOUBLE_INT, MPI_MINLOC, MPI_COMM_WORLD);
	
	for (int i = 0; i < size + 5; ++i)
	if (!rank)
		pt << res[i].a;
	else
		pt << res[i].n;
}














































//--------------------------------------------------//
//     WARNING! Altering this part of program       //
//  may cause Programming Taskbook to malfunction.  //
//--------------------------------------------------//
#ifdef PT4_CODEBLOCKS
#include "seh.h"
int main()
{
    __seh_try{Solve();}
    __seh_except(Filter(GetExceptionCode())){}
    __seh_end_except
    return 0;
}
#else
#pragma warning (disable:4430)
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    _try 
    {
         Solve();
    }
    _except(Filter(GetExceptionInformation()))
    {}  
    return 0;
}
#pragma warning (default:4430)
#endif
//--------------------------------------------------//


