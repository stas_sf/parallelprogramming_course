#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin78");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int n;
	pt >> n;

	MPI_Comm comm;
	int color = n == 0 ? MPI_UNDEFINED : 0;

	MPI_Comm_split(MPI_COMM_WORLD, color, rank, &comm);

	if (comm == MPI_COMM_NULL)
        return;

	double a;
	pt >> a;

    int s, r;
	MPI_Comm_size(comm, &s);
	MPI_Comm_rank(comm, &r);

	double rbuf[20];

	MPI_Gather(&a, 1, MPI_DOUBLE, rbuf, 1, MPI_DOUBLE, s - 1, comm);

	if (r == s - 1)
		for (int i = 0; i < s; ++i)
			pt << rbuf[i];
}
