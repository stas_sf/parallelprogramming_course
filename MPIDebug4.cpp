#include <windows.h>
#include <ctime>
#pragma hdrstop
#include "pt4.h"
#include "mpi.h"

int ProcCount = 0;
int Rank = 0;
double *A = 0, *B = 0, *C = 0;
int MatrSize = 0;
MPI_Datatype MPI_BLOCK;
MPI_Datatype MPI_COL;

void Transpose(double *&B)
{
    double t = 0; 
    for (int i = 0; i<MatrSize; i++)
    {
        for (int j = i+1; j < MatrSize; j++)
        {
            t = B[i * MatrSize + j];
            B[i * MatrSize + j] = B[j * MatrSize + i];
            B[j * MatrSize + i] = t;
        }
    }
}

void DataDistribution()
{
    MPI_Type_contiguous(MatrSize * MatrSize / ProcCount, MPI_DOUBLE, &MPI_COL);
    MPI_Type_commit(&MPI_COL);
}

void InitMatrices()
{
	if (Rank == 0)
		MatrSize = 4;
	// ������� ������� ������;
	// ��� ��������� �������� ������� ������ ������� ������,
	// ��� � �������� �� ���������, ������ �����������
	// ������� ��������� �� ������� �������������� ������
	MPI_Bcast(&MatrSize, 1, MPI_INT, 0, MPI_COMM_WORLD);
	// ��������� ������� ������ �� ���� ��������
	if (Rank == 0)
	{
		// ������� ��������� �������� ������
		srand((unsigned)time(0));
		A = new double[MatrSize * MatrSize];
		B = new double[MatrSize * MatrSize];
		C = new double[MatrSize * MatrSize];
		for (int i = 0; i < MatrSize; i++)
			for (int j = 0; j < MatrSize; j++)
			{
				A[i * MatrSize + j] = i + j / 10.0;//(double)rand() / RAND_MAX * 9;
				// ������� ������� ��������� �������
				// ��� ���������� ��������� �����: i + j / 10.;
				B[i * MatrSize + j] = -(i + j / 10.0);//-((double)rand() / RAND_MAX * 9);
				// ������� ������� ��������� �������
				// ��� ���������� ��������� �����: -(i + j / 10.);
			}
			ShowLine("������� �");
			for (int i = 0; i < MatrSize; i++)
			{
				for (int j = 0; j < MatrSize; j++)
					Show(A[i * MatrSize + j], 5);
				ShowLine();
			}
			ShowLine("������� �");
			for (int i = 0; i < MatrSize; i++)
			{
				for (int j = 0; j < MatrSize; j++)
					Show(B[i * MatrSize + j], 5);
				ShowLine();
			}
	}
}

void ParallelCalc()
{
    int ind;
    MPI_Status Status;
    int ProcPartSize = MatrSize / ProcCount;
    int ProcPartElem = ProcPartSize * MatrSize;
    double* bufA = new double[MatrSize*ProcPartSize];
    double* bufB = new double[MatrSize*ProcPartSize];
    double* bufC = new double[MatrSize*ProcPartSize];
    if (Rank == 0)
    {
        Transpose(B);
    }
    MPI_Scatter(A, 1, MPI_COL, bufA, 1, MPI_COL, 0, MPI_COMM_WORLD);
    MPI_Scatter(B, 1, MPI_COL, bufB, 1, MPI_COL, 0, MPI_COMM_WORLD);
    
    double c = 0;
    for(int i = 0; i < ProcPartSize; ++i)
    {
        for(int j = 0; j < ProcPartSize; ++j)
        {
            for(int k = 0; k < MatrSize; ++k)
                c += bufA[i*MatrSize+k] * bufB[j*MatrSize+k];
            bufC[i*MatrSize+j+ProcPartSize*Rank] = c;
            c = 0;
        }
    }
    
    int NextProc, PrevProc;
    for(int p = 1; p < ProcCount; ++p)
    {
        NextProc = Rank+1;
        if ( Rank == ProcCount-1 )
            NextProc = 0;
        PrevProc = Rank - 1;
        if (Rank == 0 )
            PrevProc = ProcCount-1;

        MPI_Sendrecv_replace(bufB, 1, MPI_COL, NextProc, 0, PrevProc, 0, MPI_COMM_WORLD, &Status);

        c = 0;
        for (int i = 0; i < ProcPartSize; ++i)
        {
            for(int j = 0; j < ProcPartSize; ++j)
            {
                for(int k = 0; k < MatrSize; ++k)
                {
                    c +=bufA[i*MatrSize+k] * bufB[j*MatrSize+k];
                }
                if (Rank - p >= 0 ) 
                    ind = Rank - p;
                else
                    ind=(ProcCount - p+Rank);
                bufC[i*MatrSize+j+ind*ProcPartSize]=c;
                c = 0;
            }
        }
    }
    MPI_Gather(bufC, ProcPartElem, MPI_DOUBLE, C, ProcPartElem, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    delete []bufA;
    delete []bufB;
    delete []bufC;
    if (Rank == 0)
    {
        Transpose(B);
    }
}

void Termination()
{
	delete[] A;
	delete[] B;
	delete[] C;
}

void TestResult()
{
	if (Rank == 0)
	{
		ShowLine("��������");
		for (int i=0; i < MatrSize; i++)
		{
			for (int j = 0; j < MatrSize; j++)
			{
				double t = 0;
				for (int k = 0; k < MatrSize; k++)
					t += A[i * MatrSize + k] * B[k * MatrSize + j];
				SetPrecision(3);
				Show(C[i * MatrSize + j]);
				Show(t);
				Show(C[i * MatrSize + j] - t);
				ShowLine();
			}
			ShowLine();
		}
	}
}

void Solve()
{
	Task("MPIDebug4");
	// ������� ���������� ��������� (4)
	int flag;
	MPI_Initialized(&flag);
	if (flag == 0)
		return;
	HideTask();
	// ������� ���� �������� ���� ���������, ����� ������� �������
	MPI_Comm_size(MPI_COMM_WORLD, &ProcCount);
	MPI_Comm_rank(MPI_COMM_WORLD, &Rank);
	/*if (ProcCount != GridSize * GridSize)
	{
		if (Rank == 0)
		{
			ShowLine("����� ��������� �� �������� ������ ���������");
			return;
		}
	}*/
	InitMatrices();
    DataDistribution();
    ParallelCalc();
	TestResult();
	Termination();
}