#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"

struct Triple {
	int a, b, c;
};

void Solve()
{
    Task("MPIBegin59");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	MPI_Datatype trpl;
	MPI_Type_contiguous(3, MPI_INT, &trpl);

	Triple sbuf[10], rbuf;
	int scounts[10], sdisps[10];
	for (int i = 0; i < size - 1; ++i)
	{
		pt >> sbuf[i].a;
		pt >> sbuf[i].b;
		pt >> sbuf[i].c;
		scounts[i + 1] = 1;
		sdisps[i + 2] = sdisps[i + 1];
	}

	//MPI_Scatterv(sbuf, scounts, trpl, &rbuf, 1, trpl, 0)

}














































//--------------------------------------------------//
//     WARNING! Altering this part of program       //
//  may cause Programming Taskbook to malfunction.  //
//--------------------------------------------------//
#ifdef PT4_CODEBLOCKS
#include "seh.h"
int main()
{
    __seh_try{Solve();}
    __seh_except(Filter(GetExceptionCode())){}
    __seh_end_except
    return 0;
}
#else
#pragma warning (disable:4430)
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    _try 
    {
         Solve();
    }
    _except(Filter(GetExceptionInformation()))
    {}  
    return 0;
}
#pragma warning (default:4430)
#endif
//--------------------------------------------------//


