#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin93");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int coord[2], sizes[2];

	if (!rank)
		pt >> sizes[0] >> sizes[1];

	MPI_Bcast(sizes, 2, MPI_INT, 0, MPI_COMM_WORLD);
	int dims[] = { sizes[0], sizes[1] }, periods[] = { 1, 0 };
	MPI_Comm comm;
	MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 0, &comm);

	if (rank < sizes[0] * sizes[1])
	{
		pt >> coord[0] >> coord[1];
		int r;
		MPI_Cart_rank(comm, coord, &r);
		pt << r;
	}
}
