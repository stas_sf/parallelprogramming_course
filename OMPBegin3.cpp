#include <windows.h>
#pragma hdrstop
#include "pt4.h"
#include "omp.h"

void Solve()
{
     Task("OMPBegin3");

	double t1, t2, npTime;
	double x;
	int n;

	pt >> x >> n;

	double f = 0;

	t1 = omp_get_wtime();

	for (int i = 1; i < n + 1; ++i)
	{
		double denom = 0;
		for (int j = i; j < n + 1; ++j)
		{
			denom += (j + pow(x + j, 1.0 / 3)) / (2 * i*j - 1);
		}
		f += 1 / denom;
	}

	t2 = omp_get_wtime();
	npTime = t2 - t1;

	pt << f;

	ShowLine("Non-parallel time: ", npTime);

	omp_set_num_threads(2);

	pt >> x >> n;

	int num;
	double g1 = 0, g2 = 0, g = 0;
	double pTime;
	t1 = omp_get_wtime();
#pragma omp parallel
	{
#pragma omp master
        {
            int num_procs = omp_get_num_procs();
			int num_threads = omp_get_num_threads();

			ShowLine("num_procs: ", num_procs);
			ShowLine("num_threads: ", num_threads);
        }
		double thr_t1, thr_t2, thr_t;
		int c = 0;
		num = omp_get_thread_num();
		thr_t1 = omp_get_wtime();
		if (num % 2 == 0)
		{
			for (int i = 2; i < n + 1; i += 2)
			{
				++c;
				double denom = 0;
				for (int j = i; j < n + 1; ++j)
				{
					denom += (j + pow(x + j, 1.0 / 3)) / (2 * i*j - 1);
				}
				g1 += 1 / denom;
			}
		}
		else
		{
			for (int i = 1; i < n + 1; i += 2)
			{
				++c;
				double denom = 0;
				for (int j = i; j < n + 1; ++j)
				{
					denom += (j + pow(x + j, 1.0 / 3)) / (2 * i*j - 1);
				}
				g2 += 1 / denom;
			}
		}
		thr_t2 = omp_get_wtime();
		thr_t = thr_t2 - thr_t1;
#pragma omp critical
		{
			Show("thread_num: ", num);
			Show("Count: ", c);
			ShowLine("Thread time: ", thr_t);
		}
	}
	t2 = omp_get_wtime();
	pTime = t2 - t1;

	g = g1 + g2;

	pt << g;

	ShowLine("Total parallel time: ", pTime);
	ShowLine("Rate: ", npTime / pTime);
}



















































//-----------------------------------------------------------//
//        ����������� ������� ��������� ���������            //
//     ����� �������� � ������������ ������ ���������!       //
//-----------------------------------------------------------//
/*int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{ _try {Solve();}
  _except (Filter(GetExceptionInformation())){};
  return 0;
}*/
//-----------------------------------------------------------//


