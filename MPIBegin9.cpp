#include <iostream>

#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"
void Solve()
{
    Task("MPIBegin9");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	int a[4];
	MPI_Status s;
	if (rank > 0)
	{
		for (int i = 0; i < 4; ++i)
			pt >> a[i];
		MPI_Send(a, 4, MPI_INT, 0, 0, MPI_COMM_WORLD);
	}
	else
	{
		for (int i = 1; i < size; ++i)
		{
			MPI_Recv(a, 4, MPI_INT, i, 0, MPI_COMM_WORLD, &s);
			for (int j = 0; j < 4; ++j)
				pt << a[j];
		}
	}

}
