#include <windows.h>
#pragma hdrstop
#include "mpi.h"
#include "pt4.h"

struct IntIntDouble {
    int a, b;
    double c;
};

void Build_derived_type(MPI_Datatype* type_ptr)
{
    int block_lengths[3];
    MPI_Aint displacements[3];
    MPI_Aint addresses[4];
    MPI_Datatype typelist[3];

    typelist[0] = MPI_INT;
    typelist[1] = MPI_INT;
    typelist[2] = MPI_DOUBLE;
    
    block_lengths[0] = block_lengths[1] = block_lengths[2] = 1;

    displacements[0] = 0;
    displacements[1] = sizeof(MPI_INT);
    displacements[2] = sizeof(MPI_INT) + displacements[1];
    
    MPI_Type_struct(3, block_lengths, displacements, typelist, type_ptr);
    
    MPI_Type_commit(type_ptr);
}

void Solve()
{
    Task("MPIBegin64");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Datatype idi;
    Build_derived_type(&idi);

    IntIntDouble sbuf, rbuf[10];

    pt >> sbuf.a;
    pt >> sbuf.c;
    pt >> sbuf.b;

    MPI_Allgather(&sbuf, 1, idi, rbuf, 1, idi, MPI_COMM_WORLD);

    for (int i = 0; i < size; ++i)
        pt << rbuf[i].a << rbuf[i].c << rbuf[i].b;

}
